<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Share extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
   
    public function __construct()
    {
         parent::__construct();
         $this->load->helper('form');
         $this->load->helper('url');
		 $this->load->helper(array('form'));
         $this->load->library('form_validation');
		 $this->load->library('session');
         $this->load->model('shares');
		 $this->load->model('companies');
    }

    /*List Companies */
	public function shares()
	{
		//$data['company']=$this->companies->listcompany();
		$this->load->view('shares');
	}

    /*Add/Edit company*/
    public function new_share($id = NULL)
	{
		$data['title'] = "New Share";
		$data['company']=$this->companies->listcompany();
		$this->load->view('new_share',$data);
	}
	 /*Save company*/
    public function save_company()
	{
		
		$id = $this->input->post('company_id');
        /* Set validation rule for name field in the form */ 
        $this->form_validation->set_rules('company_name', 'Name', 'required'); 
		$this->form_validation->set_rules('company_location', 'Location', 'required'); 
		$this->form_validation->set_rules('company_category', 'Category', 'required'); 
			
         if ($this->form_validation->run() == FALSE) { 
		 $data['company']='';
         $this->load->view('add_company',$data); 
         } 
         else { 
			$insertData = array(
				'company_name' => $this->input->post('company_name'),
				'company_location' => $this->input->post('company_location'),
				'company_category' => $this->input->post('company_category')
			);
			if($id){
				//$result = $this->companies->updateCompany($insertData,$id);
				$msg = "Company Updated Successfully";	
			}else{
				//$result = $this->companies->saveCompany($insertData);
				$msg = "Company Added Successfully";
			}
			if($result){
               $this->session->set_flashdata('item',$msg);
               redirect('Company');
			}
             
         } 
	}
	/*delete company*/
	public function deletecompany($id){
		//$result = $this->companies->delCompany($id);
		if($result){
            $this->session->set_flashdata('item',"Company deleted successfully");
            redirect('Company');
		}
	}
}
