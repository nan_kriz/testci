<?php
class Shares extends CI_Model {

	 public function __construct()
    {
         parent::__construct();
         $this->load->database();
    }
	public function saveCompany($insertData) {
       
		$result = $this->db->insert('company_tbl', $insertData);
        return $result;
	}
	public function updateCompany($insertData,$id) {
        $this->db->where('id', $id);
		$result = $this->db->update('company_tbl', $insertData);
        return $result;
	}
	public function listcompany() {
       
		$this->db->order_by('id','desc');
		$query=$this->db->get('company_tbl');
        return $query->result();
	}
	public function companyDetails($id) {
       
		$this->db->where('id',$id);
		$query=$this->db->get('company_tbl');
        return $query->row();
	}
	public function delCompany($id) {
       
		$this->db->where('id', $id);
		$result = $this->db->delete('company_tbl');
        return $result;
	}
}
?>