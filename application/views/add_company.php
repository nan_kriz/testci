<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Add Company</title>
	<link rel = "stylesheet" type = "text/css"  href = "<?php echo base_url(); ?>css/style.css">
</head>
<body>
	<h1>Add Company</h1>
	<div class="container">
		<form method = "post" action = "<?php echo base_url();?>Company/save_company">
			<?php echo validation_errors(); ?>  
			<?php echo form_open('form'); ?> 
			<div class="row">
				<div class="col-25">
					<label for="fname">Name</label>
				</div>
				<div class="col-75">
					<input type ="text" name ="company_name" value="<?php if($company){echo $company->company_name;} ?>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="fname">Location</label>
				</div>
				<div class="col-75">
					<input type= "text" name ="company_location" value="<?php if($company){echo $company->company_location;} ?>" />
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="fname">Category</label>
				</div>
				<div class="col-75">
					<input type ="text" name ="company_category" value="<?php if($company){echo $company->company_category;} ?>"/>
				</div>
			</div>
			<input type ="hidden" name ="company_id" value="<?php if($company){echo $company->id;} ?>"/>
			<div class="row">
				<input type= "submit" name ="submit" value="submit" />
			</div>
		</form>
	</div>
</body>
</html>