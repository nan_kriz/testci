<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Companys</title>
	<link rel = "stylesheet" type = "text/css"  href = "<?php echo base_url(); ?>css/liststyle.css">
</head>
<body>
	<p style="color:red;">
	<?php
	echo $this->session->flashdata('item');
	?>
	</p>
	<h1>Company List</h1>
    <a href ="<?php echo base_url();?>Company/add_company">Add New Company</a></br>
	<a href ="<?php echo base_url();?>Share/shares">Buy Shares</a>
	<table id="customers">
	  <tr>
		<th>Company Name</th>
		<th>Location</th>
		<th>Company Category</th>
		<th>Action</th>
	  </tr>
	    <?php
			if($company){
				foreach($company as $companylist){
                ?>
				<tr>
				<td><?php echo $companylist->company_name;?></td>
				<td><?php echo $companylist->company_location;?></td>
				<td><?php echo $companylist->company_category;?></td>
				<td><a href="<?php echo base_url();?>Company/add_company/<?php echo $companylist->id?>">Edit</a>/<a href="#" onclick="myFunction(<?php echo $companylist->id?>)">Delete</a></td>
				</tr>
				<?php
				}
			}
		?>
</body>
</html>
<script type="text/javascript">
    function myFunction(id){
		
		var url="<?php echo base_url();?>";
        var r=confirm("Do you want to delete this?")
        if (r==true)
          window.location = url+"Company/deletecompany/"+id;
        else
          return false;
    } 
</script>