<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Add Share</title>
	<link rel = "stylesheet" type = "text/css"  href = "<?php echo base_url(); ?>css/style.css">
</head>
<body>
    <h1>Add Share</h1>
	<div class="container">
		<form method = "post" action = "<?php echo base_url();?>Company/save_share">
			<?php echo validation_errors(); ?>  
			<?php echo form_open('form'); ?> 
			<div class="row">
				<div class="col-25">
					<label for="fname">Company Name</label>
				</div>
				<div class="col-75">
					<select name ="company_name">
							<?php
							if($company){
								foreach($company as $list){
									?>
									<option value ="<?php echo $list->id ?>"><?php echo $list->company_name ?></option>
									<?php
								}
							}
							?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="fname">Share</label>
				</div>
				<div class="col-75">
					<input type= "text" name ="company_location" />
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="fname">No of shares</label>
				</div>
				<div class="col-75">
					<input type ="text" name ="company_category" />
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="fname">Cost</label>
				</div>
				<div class="col-75">
					<input type ="text" name ="company_category" />
				</div>
			</div>
			<div class="row">
				<input type= "submit" name ="submit" value="submit" />
			</div>
		</form>
	</div>
</body>
</html>